## *Candida parapsilosis* CDC 317

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEA32889](https://www.ebi.ac.uk/ena/browser/view/PRJEA32889)
* **Assembly accession**: [GCA_000182765.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000182765.2)
* **Original submitter**: Wellcome Trust Sanger Institute

### Assembly overview

* **Assembly level**: Contig
* **Assembly name**: ASM18276v2
* **Assembly length**: 13,030,174
* **#Contig**: 9
* **Mitochondiral**: Yes
* **N50 (L50)**: 2,091,826 (3)

### Annotation overview

* **Original annotator**: Wellcome Trust Sanger Institute
* **CDS count**: 5836
* **Pseudogene count**: 26
* **tRNA count**: 91
* **rRNA count**: 4
* **Mobile element count**: 0
