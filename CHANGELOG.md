# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-04-28)

### Fixed

* Fix 134 locus_tag in tRNA features that contain forbidden characters. Ex.: CPAR2_t_cp_Arg(UCU)_30 => CPAR2_t_cp_Arg_UCU_30.

## v1.1 (2020-10-06)

### Edited

* Add missing locus_tag qualifiers in 24 tRNA features.
* Build all locus hierarchy (e.g., gene > mRNA > CDS).


## v1.0 (2020-10-05)

### Added

* The 9 annotated chromosomes of Candida parapsilosis CDC 317 (source EBI, [GCA_000182765.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000182765.2)).
